<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>


	    
<div class="container">
   	<table class="table table-striped">
   		<caption>Your todos are</caption>
   		<thead>
   			<tr>
   				<th>Description</th>
   				<th>Target Date</th>
   				<th>Is it done?</th>
   			</tr>
   		</thead>
   		<tbody>
   			<c:forEach items="${todos}" var="todo">
    			<tr>
    				<td>${todo.desc}</td>
    				<td><fmt:formatDate value="${todo.targetDate}"/></td>
    				<td>${todo.done}</td>
    				<td><a href="/update-todo?id=${todo.id}" class="btn btn-success">Update</a></td>
    				<td><a href="/delete-todo?id=${todo.id}" class="btn btn-warning">Delete</a></td>
   				</tr>
   			</c:forEach>
   		</tbody>
   	</table>
    <br>
    <div>
    	<a href="/add-todo" class="button">Add Todo</a>
    </div>	
</div>
<%@ include file="common/footer.jspf" %>
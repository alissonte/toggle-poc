package br.com.alservice.springboot.web.togglz;

import org.togglz.core.Feature;
import org.togglz.core.annotation.EnabledByDefault;
import org.togglz.core.annotation.Label;
import org.togglz.core.context.FeatureContext;

public enum EnumTogglzFeatures implements Feature{
	
	@EnabledByDefault
    @Label("MenuTodos")
	MENU_TODOS;
	
	public boolean isActive() {
        return FeatureContext.getFeatureManager().isActive(this);
	}
}

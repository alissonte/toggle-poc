package br.com.alservice.springboot.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToggleWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToggleWebApplication.class, args);
	}
}

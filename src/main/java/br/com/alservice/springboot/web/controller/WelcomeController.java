package br.com.alservice.springboot.web.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.togglz.core.manager.FeatureManager;

import br.com.alservice.springboot.web.togglz.EnumTogglzFeatures;

@Controller
public class WelcomeController {	
	
	private FeatureManager manager;
	
	public WelcomeController(FeatureManager manager) {
		this.manager = manager;
	}

	@RequestMapping(value="/", method=RequestMethod.GET)
	public String showWelcomePage(ModelMap model){
		boolean active = manager.isActive(EnumTogglzFeatures.MENU_TODOS);
		model.put("name", getLoggedInUsername());
		model.put("menuTodosActive", active);
		return "welcome";
	}
	
	private String getLoggedInUsername() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(principal instanceof UserDetails) {
			return ((UserDetails)principal).getUsername();
		}
		return principal.toString();
	}
}

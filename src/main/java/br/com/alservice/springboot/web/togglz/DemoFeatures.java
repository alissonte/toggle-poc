package br.com.alservice.springboot.web.togglz;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;
import org.togglz.core.Feature;
import org.togglz.core.manager.TogglzConfig;
import org.togglz.core.repository.FeatureState;
import org.togglz.core.repository.StateRepository;
import org.togglz.core.user.UserProvider;
import org.togglz.spring.security.SpringSecurityUserProvider;

@Component
public class DemoFeatures implements TogglzConfig {

	@Override
	public Class<? extends Feature> getFeatureClass() {
		return EnumTogglzFeatures.class;
	}

	@Override
	public StateRepository getStateRepository() {
		return customStateRepository();
	}

	@Override
	public UserProvider getUserProvider() {
		return new SpringSecurityUserProvider("ADMIN_AUTHORITY");
	}

	private StateRepository customStateRepository() {
		return new StateRepository() {
			private Map<String, FeatureState> featureStore = new ConcurrentHashMap<>();

			@Override
			public FeatureState getFeatureState(Feature feature) {
				if (featureStore.containsKey(feature.name())) {
					System.out.println(feature.name() + ": " + featureStore.get(feature.name()).isEnabled());
					return featureStore.get(feature.name());
				}
				return new FeatureState(feature, false);
			}

			@Override
			public void setFeatureState(FeatureState featureState) {
				System.out.println(featureState.getFeature().name() + ": " + featureState.isEnabled());
				featureStore.put(featureState.getFeature().name(), featureState);
			}
		};
	}

}

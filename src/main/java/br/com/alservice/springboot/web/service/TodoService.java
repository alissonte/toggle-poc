package br.com.alservice.springboot.web.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.alservice.springboot.web.model.Todo;

@Service
public class TodoService {
	
	private static List<Todo> todos = new ArrayList<>();
	private static int todoCount = 3;
	
	static {
		todos.add(new Todo(1, "alisson", "Learn Reat/Redux", new Date(), false));
		todos.add(new Todo(2, "alisson", "Learn Java", new Date(), false));
		todos.add(new Todo(3, "alisson", "Learn JPA", new Date(), false));
	}
	
	public List<Todo> retrieveTodos(String user){
		List<Todo> result = todos.stream()
                .filter(todo -> user.equals(todo.getUser()))
                .collect(Collectors.toList());
		
		return result;
	}
	
	public Todo retrieveTodo(int id){
		Todo result = todos.stream()
				  .filter(todo -> id == todo.getId())
				  .findAny()
				  .orElse(null);
		return result;
	}
	
	public void addTodo(String name, String desc, Date targetDate, boolean isDone) {
		todos.add(new Todo(++todoCount, name, desc, targetDate, isDone));
	}
	
	public void updateTodo(Todo todo) {
		todos.remove(todo);
		todos.add(todo);
	}
	
	public void deleteTodo(int id) {
		todos.removeIf(todo -> id == todo.getId());
	}
}
